#include <Rcpp.h>
#include <string>

// [[Rcpp::export]]
int hamming(const std::string& str1, const std::string& str2) {
  int sdist = 0;

  for(int i = 0; i < str1.length(); ++i) {
    if(str1[i] != str2[i]) {
      sdist++;
    }
  }
  return(sdist);
}
