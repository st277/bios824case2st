## BIOS824Case2ST

This is a README file for the bios824case2st package.
First, Devtools, ShortRead and Rcpp pakcages are required to be installed before this package is implemented.
```{r}
source("http://www.bioconductor.org/biocLite.R")
biocLite(c("ShortRead"))
install.packages("Rcpp")
install.packages("devtools")
```

Then intall bios824case2st package. It takes a little bit time. 
```{r}
devtools::install_gitlab(repo = "st277/bios824case2st",host = "https://gitlab.oit.duke.edu",build = TRUE,build_opts = c("--no-resave-data"))
library(bios824case2st)
```

Finally, play with the package!!! 
```{r}
table <- mapping_hamming("~/my.gen.fastq","~/my.gen.fasta",2)
head(table$proportions)
head(table$mapping)
```

For more information about this package, please go to the vignette and the help pages. 
```{r}
vignette("my-vignette","bios824case2st")
```

Thank you for using this package!:blush:
